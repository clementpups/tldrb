﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TLDR.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class TLDRController : ControllerBase
    {
        private string baseUrl = "http://localhost:8888/";

        // GET: <TLDRController>
        [HttpGet]
        public async Task<string> Get()
        {
            return await this.GetAllTLDR();
        }

        // GET <TLDRController>/5
        [HttpGet("{id}")]
        public async Task<string> Get(string id)
        {
            return await this.GetTLDRById(id);
        }

        // POST <TLDRController>
        [Authorize]
        [HttpPost]
        public async void Post([FromBody] TLDR value)
        {
            await this.CreateTLDR(Newtonsoft.Json.JsonConvert.SerializeObject(value));
        }

        // PUT <TLDRController>/5
        [Authorize]
        [HttpPut("{id}")]
        public async void Put(int id, [FromBody] TLDR value)
        {
            await this.UpdateTLDR(Newtonsoft.Json.JsonConvert.SerializeObject(value));
        }

        // DELETE <TLDRController>/5
        [Authorize]
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }


        //Elastic Search call API
        private async Task<string> GetAllTLDR()
        {
            string apiResponse = string.Empty;
            using (HttpClient client = new HttpClient())
            {
                using (var response = await client.GetAsync(baseUrl + "TLDR/"))
                {
                    apiResponse = await response.Content.ReadAsStringAsync();
                }
            }
            return apiResponse = "test";
        }

        private async Task<string> GetTLDRById(string id)
        {
            string apiResponse = string.Empty;
            using (HttpClient client = new HttpClient())
            {
                using (var response = await client.GetAsync(baseUrl + "TLDR/" + id))
                {
                    apiResponse = await response.Content.ReadAsStringAsync();
                }
            }
            return apiResponse = "test2";
        }

        private async Task<string> CreateTLDR(string value)
        {
            string apiResponse = string.Empty;
            var httpContent = new StringContent(value, Encoding.UTF8, "application/json");
            using (HttpClient client = new HttpClient())
            {
                using (var response = await client.PostAsync(baseUrl + "TLDR/", httpContent))
                {
                    apiResponse = await response.Content.ReadAsStringAsync();
                }
            }
            return apiResponse = "test3";
        }

        private async Task<string> UpdateTLDR(string value)
        {

            string apiResponse = string.Empty;
            var httpContent = new StringContent(value, Encoding.UTF8, "application/json");
            using (HttpClient client = new HttpClient())
            {
                using (var response = await client.PutAsync(baseUrl + "TLDR/", httpContent))
                {
                    apiResponse = await response.Content.ReadAsStringAsync();
                }
            }
            return apiResponse = "test3";

        }
    }
}
